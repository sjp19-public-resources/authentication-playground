import { Home } from "./Home";
import { AuthProvider } from "@galvanize-inc/jwtdown-for-react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap-icons/font/bootstrap-icons.css";
import "./overrides.css";
import TitleBar from "./TitleBar";
import SignupForm from "./SignupForm";
import LoginForm from "./LoginForm";

const PUBLIC_URL = process.env.REACT_APP_PUBLIC_URL;
const API_URL = process.env.REACT_APP_USER_SERVICE_API_HOST;
if (!PUBLIC_URL) {
  throw Error("REACT_APP_PUBLIC_URL is not set");
}
if (!API_URL) {
  throw Error("REACT_APP_USER_SERVICE_API_HOST is not set");
}

function App() {
  const domain = /http(s?):\/\/[^/]+/;
  const basename = PUBLIC_URL.replace(domain, "");
  return (
    <div className="container">
      <BrowserRouter basename={basename}>
        <AuthProvider baseUrl={API_URL}>
          <TitleBar />
          <Routes>
            <Route index={true} path="/" element={<Home />}></Route>
            <Route index={true} path="/signup" element={<SignupForm />}></Route>
            <Route index={true} path="/login" element={<LoginForm />}></Route>
          </Routes>
        </AuthProvider>
      </BrowserRouter>
    </div>
  );
}

export default App;
