import LoginForm from "./LoginForm";
import "bootstrap/dist/js/bootstrap.bundle";
import "react-json-pretty/themes/monikai.css";
import TokenCard from "./TokenCard";
import UserDataCard from "./UserDataCard";
import useToken from "@galvanize-inc/jwtdown-for-react";

const ConsoleBanner = () => {
    return (
        <div className="alert alert-info mt-3 mb-3" role="alert">
            <i className="bi bi-info-circle-fill"></i> Open your browser's console to
            see more information.
        </div>
    );
};

export const Home = () => {
    const { token } = useToken();
    return (
        <div>
            <ConsoleBanner />
            {!token && <LoginForm />}
            {token && <TokenCard />}

            <UserDataCard />
        </div>
    );
};
